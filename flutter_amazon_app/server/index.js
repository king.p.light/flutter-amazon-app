// import package
const express = require("express");
const mongoose = require("mongoose");
// import each router
const authRouter = require("./routes/auth.js");
const adminRouter = require("./routes/admin.js");

// init
const PORT = 3000;
const app = express();
const DB =
  "mongodb+srv://admin:admin1234@cluster0.2njb3.mongodb.net/flutter-amazon-app?retryWrites=true&w=majority";

// middleware (client -> middleware -> server -> client)
app.use(express.json());
app.use(authRouter);
app.use(adminRouter);

// connections
mongoose
  .connect(DB)
  .then(() => {
    console.log("Connection Successful!");
  })
  .catch((error) => {
    console.log({ error: error.message });
  });

app.listen(PORT, "0.0.0.0", () => {
  console.log(`Connected at port: ${PORT}`);
});
