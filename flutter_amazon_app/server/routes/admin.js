const express = require("express");
const adminRouter = express.Router();
const admin = require("../middleware/admin");
const Products = require("../models/products");

// create admin middleware
adminRouter.post("/admin/add-product", admin, async (req, res) => {
  try {
    const { productName, description, price, quantity, images, category } =
      req.body;

    let products = new Products({
      productName,
      description,
      price,
      quantity,
      images,
      category,
    });

    products = await products.save();
    res.json(products);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// get all products
adminRouter.get("/admin/get-products", admin, async (req, res) => {
  try {
    const products = await Products.find({});
    res.json(products);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

module.exports = adminRouter;
