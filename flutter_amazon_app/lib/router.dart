import 'package:flutter/material.dart';
import 'package:flutter_amazon_app/common/widgets/bottom_bar.dart';
import 'package:flutter_amazon_app/features/admin/screens/add_product_screen.dart';
import 'package:flutter_amazon_app/features/auth/screens/auth_screen.dart';
import 'package:flutter_amazon_app/features/home/screens/home_screen.dart';

Route<dynamic> generateRoute(RouteSettings routeSettings) {
  switch (routeSettings.name) {
    case AuthScreen.routeName:
      return MaterialPageRoute(
        builder: (_) => const AuthScreen(),
      );
    case HomeScreen.routeName:
      return MaterialPageRoute(
        builder: (_) => const HomeScreen(),
      );
    case BottomBar.routeName:
      return MaterialPageRoute(
        builder: (_) => const BottomBar(),
      );
    case AddProductScreen.routeName:
      return MaterialPageRoute(
        builder: (_) => const AddProductScreen(),
      );

    default:
      return MaterialPageRoute(
        settings: routeSettings,
        builder: (_) => const Scaffold(
          body: Center(
            child: Text('Screen does not exits!'),
          ),
        ),
      );
  }
}
