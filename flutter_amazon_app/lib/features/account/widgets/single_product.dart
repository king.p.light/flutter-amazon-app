import 'package:flutter/material.dart';
import 'package:flutter_amazon_app/constants/global_variables.dart';

class SingleProduct extends StatelessWidget {
  final String image;

  const SingleProduct({Key? key, required this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5.0),
      child: DecoratedBox(
        decoration: BoxDecoration(
          border: Border.all(
            width: 1.5,
            color: Colors.black12,
          ),
          borderRadius: BorderRadius.circular(5.0),
          color: Colors.white,
        ),
        child: Container(
          width: 180.0,
          padding: const EdgeInsets.all(6.0),
          child: Image.network(
            image,
            fit: BoxFit.fitHeight,
            width: 180.0,
          ),
        ),
      ),
    );
  }
}
