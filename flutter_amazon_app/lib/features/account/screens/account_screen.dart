import 'package:flutter/material.dart';
import 'package:flutter_amazon_app/constants/global_variables.dart';
import 'package:flutter_amazon_app/features/account/widgets/below_app_bar.dart';
import 'package:flutter_amazon_app/features/account/widgets/orders.dart';
import 'package:flutter_amazon_app/features/account/widgets/top_button.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50.0),
        child: AppBar(
          flexibleSpace: Container(
            decoration: const BoxDecoration(
              gradient: GlobalVariables.appBarGradient,
            ),
          ),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                alignment: Alignment.topLeft,
                child: Image.asset(
                  'assets/images/amazon_in.png',
                  width: 120.0,
                  height: 45.0,
                  color: GlobalVariables.backgroundColor,
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                child: Row(
                  children: const [
                    Padding(
                      padding: EdgeInsets.only(right: 15.0),
                      child: Icon(
                        Icons.notifications_rounded,
                        color: GlobalVariables.backgroundColor,
                      ),
                    ),
                    Icon(
                      Icons.search_rounded,
                      color: GlobalVariables.backgroundColor,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      body: Column(
        children: const [
          BelowAppBar(),
          SizedBox(height: 10.0),
          TopButton(),
          SizedBox(height: 20.0),
          Orders(),
        ],
      ),
    );
  }
}
