import 'package:flutter/material.dart';
import 'package:flutter_amazon_app/constants/global_variables.dart';
import 'package:flutter_amazon_app/provider/user_provider.dart';
import 'package:provider/provider.dart';

class AddressBox extends StatelessWidget {
  const AddressBox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context).user;

    return Container(
      height: 40.0,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(166, 12, 20, 1),
            Color.fromRGBO(175, 14, 24, 1),
          ],
          stops: [0.5, 1.0],
        ),
      ),
      padding: const EdgeInsets.only(left: 10.0),
      child: Row(
        children: [
          const Icon(
            Icons.location_on_outlined,
            size: 20.0,
            color: GlobalVariables.backgroundColor,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Text(
                'Delivery to ${user.username} - ${user.address}',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: GlobalVariables.backgroundColor,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(
              right: 5.0,
            ),
            child: Icon(
              Icons.arrow_drop_down_rounded,
              size: 24.0,
              color: GlobalVariables.backgroundColor,
            ),
          ),
        ],
      ),
    );
  }
}
