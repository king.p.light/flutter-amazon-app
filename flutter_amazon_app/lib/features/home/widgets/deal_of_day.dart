import 'package:flutter/material.dart';
import 'package:flutter_amazon_app/constants/global_variables.dart';

class DealOfDay extends StatefulWidget {
  const DealOfDay({Key? key}) : super(key: key);

  @override
  State<DealOfDay> createState() => _DealOfDayState();
}

class _DealOfDayState extends State<DealOfDay> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          alignment: Alignment.topLeft,
          padding: const EdgeInsets.only(left: 10.0, top: 15.0),
          child: const Text(
            'Deal of the day',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
          ),
        ),
        SizedBox(height: 4.0),
        Image.network(
          height: 235.0,
          fit: BoxFit.fitHeight,
          'https://images.unsplash.com/photo-1526925712774-2833a7ecd0d4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1274&q=80',
        ),
        SizedBox(height: 4.0),
        Container(
          alignment: Alignment.topLeft,
          padding: const EdgeInsets.only(left: 15.0),
          child: const Text(
            '\$100',
            style: const TextStyle(fontSize: 18.0),
          ),
        ),
        SizedBox(height: 4.0),
        Container(
          alignment: Alignment.topLeft,
          padding: const EdgeInsets.only(left: 15.0, top: 5.0, right: 40.0),
          child: const Text(
            'Apple MacBook Pro M1',
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.network(
                width: 100.0,
                height: 100.0,
                fit: BoxFit.fitWidth,
                'https://images.unsplash.com/photo-1615750185825-fc85c6aba18d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
              ),
              Image.network(
                width: 100.0,
                height: 100.0,
                fit: BoxFit.fitWidth,
                'https://images.unsplash.com/photo-1615750185825-fc85c6aba18d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
              ),
              Image.network(
                width: 100.0,
                height: 100.0,
                fit: BoxFit.fitWidth,
                'https://images.unsplash.com/photo-1615750185825-fc85c6aba18d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
              ),
              Image.network(
                width: 100.0,
                height: 100.0,
                fit: BoxFit.fitWidth,
                'https://images.unsplash.com/photo-1615750185825-fc85c6aba18d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
              ),
            ],
          ),
        ),
        Container(
          padding:
              const EdgeInsets.symmetric(vertical: 15.0).copyWith(left: 15.0),
          alignment: Alignment.topLeft,
          child: Text(
            'See all deals',
            style: TextStyle(color: GlobalVariables.selectedNavBarColor),
          ),
        ),
      ],
    );
  }
}
